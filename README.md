# lufi_theme

Disroot theme for Lufi. This is a fork of Lufi's default theme with small changes to colors and possible few additions here and there.

To use this theme:
  - `git clone` it in `/var/www/lufi/themes/`
  - Edit `lufi.conf` and change the theme: `theme => 'beetroot',`

Our custom changes to the theme are currently maintained in a separate file at: `public/css/beetroot.css`. This theme is usable as is but it is still work in progress. 
